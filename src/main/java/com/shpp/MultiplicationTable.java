package com.shpp;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.Double.parseDouble;

class MultiplicationTable {
    private static final Logger logger = LogManager.getLogger(MultiplicationTable.class);

    private MultiplicationTable() {
    }

    static void run(String type) {
        logger.info("Start program!");
        Properties properties = getProperty();
        if (properties == null) {
            logger.fatal("Property not found!");
            return;
        }
        double min = 0;
        double max = 0;
        double increment = 0;

        try {
            min = parseDouble(properties.getProperty("min"));
            max = parseDouble(properties.getProperty("max"));
            increment = parseDouble(properties.getProperty("increment"));
        } catch (NullPointerException e) {
            logger.fatal("Some of the properties parameters are not specified!", e);
            return;
        }

        logger.info("Min = {}, max = {}, increment = {}.", min, max, increment);

        makeTable(min, max, increment, type);
    }

    private static void makeTable(double min, double max, double increment, String type) {
        if (checkType(type)) {
            logger.info("----------");
            for (double i = min; i <= max; i += increment) {
                for (double j = min; j <= max; j += increment) {
                    logItem(i, j, type);
                }
                logger.info("----------");
            }
            logger.info("End program!");
        } else {
            logger.error("Incorrect data type!");
        }
    }


    private static void logItem(double i, double j, String type) {
        if (type.equals("double") || type.equals("float")) {
            logger.info(i + " * " + j + " = " + i * j);
        }
        if (type.equals("int") || type.equals("short") || type.equals("byte")) {
            long a = (long) i;
            long b = (long) j;
            logger.info(a + " * " + b + " = " + a * b);
        }
    }


    private static Properties getProperty() {
        Properties property;
        try {
            InsideProperty insideProperty = new InsideProperty("config.properties");
            property = insideProperty.getProperty();
            logger.debug("Return inside properties!");
            return property;
        } catch (IOException e) {
            logger.error(e);
        }
        return null;
    }

    static boolean checkType(String type) {
        if(type.equals("int") || type.equals("short") || type.equals("byte")
                || type.equals("double") || type.equals("float")){
            return true;
        } else {
            return false;
        }
    }
}
