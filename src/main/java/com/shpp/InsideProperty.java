package com.shpp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class InsideProperty {
        private Properties property = new Properties();

        public InsideProperty(String propertyPathAndName) throws IOException {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(propertyPathAndName);
            property.load(inputStream);
            inputStream.close();
        }

        Properties getProperty() {
            return property;
        }
}
