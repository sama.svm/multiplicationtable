package com.shpp;


public class App {
    public static void main(String[] args) {
        String type;
        if (System.getProperty("myType") == null) {
            type = "int";
        } else {
            type = System.getProperty("myType");
        }
        MultiplicationTable.run(type);
    }
}
