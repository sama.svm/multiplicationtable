package com.shpp;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Properties;

import static java.lang.Double.parseDouble;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    void TestProperties(){
        InsideProperty insideProperty = null;
        try {
           insideProperty = new InsideProperty("config.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Properties properties = insideProperty.getProperty();

        assertTrue(parseDouble(properties.getProperty("min")) > 0);
        assertTrue(parseDouble(properties.getProperty("max")) > 0);
        assertTrue(parseDouble(properties.getProperty("increment")) > 0);
    }

}
